/* eslint-disable node/no-unpublished-require */
const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("ERC20", function () {
  let owner, addr1, addr2;
  let erc20Token;
  let token;
  before(async function () {
    [owner, addr1, addr2] = await ethers.getSigners();
    erc20Token = await ethers.getContractFactory("ERC20");
    token = await erc20Token.deploy("Danny", "DAN");
    await token.deployed();
  });

  it("Should return expected name", async function () {
    expect(await token.name()).to.equal("Danny");
  });

  it("Should return expected symbol", async function () {
    expect(await token.symbol()).to.equal("DAN");
  });

  it("Should return expected decimals", async function () {
    expect(await token.decimals()).to.equal(18);
  });

  it("Should return expected totalSupply", async function () {
    expect(await token.totalSupply()).to.equal(0);
  });

  it("Should return owner balance", async function () {
    const ownerBalance = await token.balanceOf(owner.address);
    expect(await token.totalSupply()).to.equal(ownerBalance);
  });

  it("Should mint", async function () {
    const minting = await token.mint(10000);
    await minting.wait();
    expect(minting).to.emit(token, "Transfer");
    expect(await token.totalSupply()).to.equal(10000);
    const ownerBalance = await token.balanceOf(owner.address);
    expect(await token.totalSupply()).to.equal(ownerBalance);
  });

  it("Should not mint", async function () {
    try {
      const minting = await token.connect(addr1).mint(10000);
      await minting.wait();
    } catch (error) {
      expect(error.message).to.equal(
        "VM Exception while processing transaction: reverted with reason string 'ERC20: only deployer can mint'"
      );
    }
    // await expect(await token.connect(addr1).mint(10000)).to.be.revertedWith("MyCustomError");
  });

  it("Should transfer", async function () {
    expect(await token.transfer(addr1.address, 500)).to.emit(token, "Transfer");
    expect(await token.balanceOf(addr1.address)).to.equal(500);
    expect(await token.balanceOf(owner.address)).to.equal(9500);

    await token.connect(addr1).transfer(addr2.address, 250);
    expect(await token.balanceOf(addr2.address)).to.equal(250);
    expect(await token.balanceOf(addr1.address)).to.equal(250);
  });

  it("Should burn", async function () {
    const burning = await token.connect(addr1).burn(100);
    await burning.wait();
    expect(await token.balanceOf(addr1.address)).to.equal(150);
  });

  it("Should transferFrom", async function () {
    await token.approve(addr2.address, 5000);
    expect(await token.allowance(owner.address, addr2.address)).to.equal(5000);
    await token.connect(addr2).transferFrom(owner.address, addr1.address, 2000);
    expect(await token.allowance(owner.address, addr2.address)).to.equal(3000);
    expect(await token.balanceOf(addr1.address)).to.equal(2150);
    expect(await token.balanceOf(owner.address)).to.equal(7500);

    await token.approve(addr2.address, 0);
    expect(await token.allowance(owner.address, addr2.address)).to.equal(0);
  });

  it("Should decreaseAllowance", async function () {
    await token.approve(addr2.address, 5000);
    expect(await token.allowance(owner.address, addr2.address)).to.equal(5000);
    await token.decreaseAllowance(addr2.address, 1000);
    expect(await token.allowance(owner.address, addr2.address)).to.equal(4000);
  });

  it("Should increaseAllowance", async function () {
    await token.increaseAllowance(addr2.address, 5000);
    expect(await token.allowance(owner.address, addr2.address)).to.equal(9000);
    await token.approve(addr2.address, 0);
    expect(await token.allowance(owner.address, addr2.address)).to.equal(0);
  });
});
